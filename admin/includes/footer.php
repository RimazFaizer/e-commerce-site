</div>

<footer class="text-center" id="footer">
&copy; copyright 2013-2015
  Genuine Auto Center</footer>

  <script>
    function get_child_options(selected){
      if(typeof selected === undefined){
        var selected = '';
      }
      var parentID = jQuery('#parent').val();
      jQuery.ajax({
        url: '/e-commerce site/admin/parsers/child_categories.php',
        type: 'POST',
        data: {parentID : parentID, selected: selected},
        success: function(data){
          jQuery('#child').html(data);
        },
        error: function(){alert("Something went wrong with the child options.")},
      });
    }
    jQuery('select[name="parent"]').change(function(){
      get_child_options();
    });
  </script>

</body>
</html>
