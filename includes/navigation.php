<?php
$sql = "SELECT * FROM categories WHERE parent = 0";
$pquery = $db->query($sql);
 ?>
 
<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <a href="index.php" class="navbar-brand">Genuine Auto Parts</a>
      <ul class="nav navbar-nav">
        <?php while($parent = mysqli_fetch_assoc($pquery)) : ?>
          <?php
          $parent_id = $parent['id'];
          $sql2 = "SELECT * FROM categories WHERE parent ='$parent_id'";
          $cquery = $db->query($sql2);
          ?>
          <!--Menu Items -->
       <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            <?php echo $parent['category']; ?>
            <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <?php while($child = mysqli_fetch_assoc($cquery)) : ?>
              <li><a href="index.php?cat=<?= $child['id']; ?>"><?php echo $child['category']; ?></a></li>
            <?php endwhile; ?>
            </ul>
      </li>
    <?php endwhile; ?>
    </ul>
    <form action="index.php" method="get" class="nav navbar-nav navbar-form navbar-right">
      <div class="form-group input-group">
        <input type="text" name="s" id="s" class="form-control" style="width: 300px;" placeholder="Search Products..." />
        <div class="input-group-btn">
        <button class="btn btn-defult">
          <i class="glyphicon glyphicon-search"></i>
        </button>
        </div>
      </div>
      </form>
      </div>
      </nav>
