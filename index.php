 <?php
 require_once 'core/init.php';
 include 'includes/head.php';
 include 'includes/navigation.php';
 include 'includes/headerfull.php';
//  include 'includes/leftbar.php';

if(isset($_GET['cat'])) {
  $cat = (int)$_GET['cat'];
  $sql = "SELECT * FROM products WHERE categories=$cat AND featured = 1 AND deleted = 0";
}
else if(isset($_GET['s'])) {
  $s = $_GET['s'];
  $sql = "SELECT * FROM products WHERE title LIKE '%$s%' AND featured = 1 AND deleted = 0";
}
else {
  $sql = "SELECT * FROM products WHERE featured = 1 AND deleted = 0";
}

 $featured = $db->query($sql);
 ?>



        <!-- main content -->
        
        <!-- <div class="well"> -->
          <br><br>
        <div class="panel panel-info">
          <div class="panel-heading main-panel-head">Featured products</div>
          <div class="panel-body">
            <div class="row">
              <!-- <h2 class="text-center index-head">Featured products</h2> -->
              <hr>
                <?php while($product = mysqli_fetch_assoc($featured)) : ?>
              <div class="col-sm-3 text-center prod-card">
                <div>
                  <h4><?= $product['title']; ?> </h4>
                  <img src="<?= $product['image']; ?>" alt="<?= $product['title'];
                  ?>" class="img-responsive img-thumb"/>
                  <p class="list price text-danger">List Price: <s>$<?= $product
                  ['list_price']; ?></s></p>
                  <p class="price">Our Price: $<?= $product['price']; ?></p>
                  <button type="button" class="btn btn-block btn-info"
                onclick="detailsmodal(<?= $product['id']; ?>)">Details</button>
                </div>
              </div>
            <?php endwhile; ?>
            </div>
          </div>
        </div>
        <!-- </div> -->

<?php
// include 'includes/rightbar.php';
include 'includes/footer.php';
?>
